# Tarefa da Minhoca
Primeiramente certifique-se de que o ambiente que você pretende executar a aplicação é o ambiente Linux. Se esse for o caso siga os passos a seguir:

1. Verifique se o servidor Apache está apropriadamente instalado em sua máquina executando o seguinte comado: `which apache2`. Caso a saída seja algo parecido com isso: `/usr/sbin/apache2` o Apache já está devidamente instalado. Caso o contrário execute o seguinte comando para instalar o server: `sudo apt-get install apache2`.
2. Terminado o passo 1 execute o seguinte comando `sudo service apache2 status` para verificar se o servidor está realmente ativo. Caso ele não esteja execute `sudo service apache2 start`.
3. Finalmente execute o comando `sudo apt-get install php7.0` para instalar o PHP, caso ele já esteja instalado ignore esta etapa.
4. Agora com todas as dependências já instaladas clone o repositório executando `git clone https://jr_ramisch@bitbucket.org/jr_ramisch/worm-app.git` e em seguida mova o diretório da aplicação para o local dos arquivos do servidor, executando: `sudo mv worm-app /var/www`. Feito isso, é necessário alterar o usuário que possui permissão para alterar o diretório movido, para isso execute: `sudo chown -R $USER:$USER /var/www/worm-app/`.
5. Finalizado o passo 4 é necessário adicionar a permissão de leitura para o diretório `/var/www`, assim execute o comando `sudo chmod -R 755 /var/www`.
6. Nessa etapa será necessário a criação de um Virtual Host< para "hospedar" a aplicação. Para isso será preciso fazer uma cópia do arquivo de configuração do PHP e nomeá-lo com a url que será utilizada para acessar a aplicação na barra de pesquisa do Browser, sabendo isso execute: `sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/localhost.conf`.
7. Após feito a cópia do arquivo de configuração, cole o seguinte conteúdo no arquivo `localhost.conf`:

      `<VirtualHost *:80>`

            ServerName localhost
            ServerAdmin admin@localhost
            DocumentRoot /var/www/worm-app
            ErrorLog ${APACHE_LOG_DIR}/error.log
            CustomLog ${APACHE_LOG_DIR}/access.log combined
      `</VirtualHost>`
      
  Feito isso execute o comando `sudo a2ensite localhost.conf` para possibilitar que o servidor Apache consiga encontrar o novo site configurado, por fim para ativar a nova configuração execute `systemclt reload apache2` e pronto o ambiente foi configurado com sucesso.
  