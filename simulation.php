<?php
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        
        $dist = $_GET["distance"];
        $subida = $_GET["climb"];
        $descida = $_GET["fall"];

        $posInicial = 0;
        $subidas = 1;
        $quantasSubidas = [];
        $arrayIndex = 0;

        while ($posInicial < $dist)
        {
            $posInicial += $subida - $descida;
            $quantasSubidas[$arrayIndex] = $subidas;
            $arrayIndex++;
            $subidas++;
        }

        echo json_encode($quantasSubidas);
    }
?>