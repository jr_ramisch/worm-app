$(document).ready(function()
{
    function animation(quantasSubidas, dist, velocidade, index)
    {
        setTimeout(function()
        {

            distPercorrida = quantasSubidas[index] * velocidade;
            $(".climbs").html("Subidas: " + quantasSubidas[index]);

            if (chegouNaMetade(distPercorrida, dist)) 
            {
                $(".yellowFrame").css("display", "block");
            }

            if (distPercorrida < dist)
            {
                animation(quantasSubidas, dist, velocidade, index+=1);
            }

            else
            {
                $(".yellowFrame").css("display", "none");
                $(".greenFrame").css("display", "block");
            }
        }, 1000);

    }

    function chegouNaMetade(distPercorrida, dist)
    {
        return (distPercorrida >= dist / 2);
    }

    function podeSairDoBuraco(velocidade) 
    {
        return velocidade > 0;
    }

    $("#btn-init").click(function() {
        
        var dist = $("#distance").val();
        var subida = $("#climb").val();
        var descida = $("#fall").val();
        var velocidade = subida - descida;

        $(".yellowFrame").css("display", "none");
        $(".greenFrame").css("display", "none");
        $(".warning").css("display", "none");

        if (podeSairDoBuraco(velocidade)) 
        {            
            $.ajax({
                url:"simulation.php",
                method:"GET",
                data:{
                    distance : dist,
                    climb : subida,
                    fall: descida
                },
                dataType:"JSON",
                
                success : function(data) {
                    animation(data, dist, velocidade, 0);
                },

                fail : function(data) {
                    alert("Um erro ocorreu.");
                }
            });
        }

        else
        {
            $(".warning").css("display", "block");
        }

    });
});


