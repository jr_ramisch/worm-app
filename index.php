<html>

	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="css/index.css"/>
		<title> Minhoca </title>
	</head>

	<body>

		<div class="bg">
			<div>
				<p class="climbs">Subidas: </p>
			</div>
			<div class="yellowFrame"></div>
			<div class="greenFrame"></div>
			<div class = "input-block">
				<label>Tamanho do Buraco</label> <br>
				<input type="text" id="distance" value = "20" placeholder="Tamanho do Buraco"/> <br>
				<label>Subida</label> <br>
				<input type="text" id="climb" value = "5" placeholder="Subida"/> <br>
				<label>Descida</label> <br>
				<input type="text" id="fall" value = "3" placeholder="Descida"/> <br>
				<p class="warning">A minhoca nunca sairá do buraco!</p>
				<input id="btn-init" type="button" value="Iniciar">
			</div>
		</div>

	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/index.js"></script>
	</body>

</html>